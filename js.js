


window.addEventListener("DOMContentLoaded",()=>
{
//------------------tab---------------------------------------------


const tabs = document.querySelectorAll('.tabheader__item'),
      tabsContent = document.querySelectorAll('.tabcontent'),
      tabsParent = document.querySelector('.tabheader__items');


function hidetabs() {               // прячет 
    tabsContent.forEach(tab=> {
        tab.classList.add('hide')
        tab.classList.remove("show","fade")
        
    });
    tabs.forEach(tab=>{
        tab.classList.remove("tabheader__item_active");


      
    });


}
function showtab(i=0){ // показывает
    
    tabsContent[i].classList.add('show', 'fade')
    tabsContent[i].classList.remove("hide")
    tabs[i].classList.add("tabheader__item_active");
    
}
hidetabs()
showtab()
tabsParent.addEventListener('click',(event) =>{


    const target= event.target;

    if  (target && target.classList.contains("tabheader__item")){
        tabs.forEach((tab, i) => {
            if (target==tab){
                hidetabs();
                showtab(i);
            }
       });

    }


    });


 


    //--------------------------------timer---------------------

    const deadline = '2023-01-01';
    function TimeRemaining (endtime) {
        const t = Date.parse(endtime)-Date.parse(new Date()),
        days= Math.floor(t/(1000*60*60*24)),
        hours =Math.floor((t/(1000*60*60))%24),
        min = Math.floor((t/(1000*60))%60),
        sec =Math.floor((t/1000)%60)


        return {
            'time': t,
            'days': days,
            'hours': hours,
            'min': min,
            'sec': sec

        };
    }

    function getZero(num){
        if(num>=0 && num<10) {
            return `0${num}`;
        }
        else{
            return num
    }
    }


    function Clock(selector, endtime) {

        const timer = document.querySelector(selector),
              days =timer.querySelector('#days'),
              hours =timer.querySelector('#hours'),
              min =timer.querySelector('#minutes'),
              sec =timer.querySelector('#seconds'),
              timeInterval=setInterval(UpdateClock, 1000);




        function  UpdateClock(){
            const t = TimeRemaining(endtime);

            days.textContent = getZero(t.days);
            hours.innerHTML = getZero(t.hours);
            min.textContent = getZero(t.min);
             sec.innerHTML = getZero(t.sec);



            if(t.total <=0){

                clearInterval(timeInterval);
            }
        }
    }


Clock('.timer', deadline);


//-----------------modal



const modalOpen = document.querySelectorAll('[open-modal]'),
      modal=document.querySelector('.modal'),
      madalClose=document.querySelector('[close]')
const madalTimer=setTimeout(OpenModal, 5000)

      function OpenModal(){
        modal.classList.add('show')
        modal.classList.remove('hide')
        clearInterval(madalTimer)
      }

      function CloseMoal(){
        modal.classList.remove('show')
        modal.classList.add('hide')
      }

      function Scroll(){
        if (window.pageYOffset+document.documentElement.clientHeight >=document.documentElement.scrollHeight){
            OpenModal()
            window.removeEventListener('scroll', Scroll)
        }
      }
     

      modalOpen.forEach(btn => {
      btn.addEventListener('click',OpenModal)
        
  
      });

      
    madalClose.addEventListener('click',()=>{
        CloseMoal()

      });

      modal.addEventListener('click',(n)=>{

            if (n.target === modal){
                modal.classList.remove('show')
                modal.classList.add('hide')


            }

    


      });
      document.addEventListener('keydown',(n)=>{

        if (n.code === "Escape" && modal.classList.contains('show')){
            modal.classList.remove('show')
            modal.classList.add('hide')


        }

  });

window.addEventListener('scroll', Scroll);

//---------------Class + rest


class MenuCard {
    constructor(src, alt, title, descr, price, parentSelector, ...classes){

        this.src=src;
        this.alt=alt;
        this.title=title;
        this.descr=descr;
        this. price= price;
        this. classes= classes ;
        this.parent =document.querySelector(parentSelector);
        this.transfer = 27;
        this.changeToUAH();
    }


    changeToUAH(){
        this. price=this. price*this.transfer ;

    }

    render(){
        const el= document.createElement('div');
       

        if(this.classes.length == 0 ){
            this.el ='menu__item';
            el.classList.add(this.el);
        } else{
            this.classes.forEach(className => el.classList.add(className));
        }


            el.innerHTML=
           `
            <img src=${this.src} alt=${this.alt}>
            <h3 class="menu__item-subtitle">${this.title}</h3>
            <div class="menu__item-descr">${this.descr}</div>
            <div class="menu__item-divider"></div>
            <div class="menu__item-price">
                <div class="menu__item-cost">Цена:</div>
                <div class="menu__item-total"><span>${this.price}</span> грн/день</div>
            </div>
        `;
        this.parent.append(el); 
            
            
    }
}

        new MenuCard(
            "img/tabs/vegy.jpg",
            "vegy",
            'Меню "Фитнес"',
            `Меню "Фитнес" - это новый подход к приготовлению блюд: больше свежих овощей и фруктов. 
            Продукт активных и здоровых людей. 
            Это абсолютно новый продукт с оптимальной ценой и высоким качеством!`,
            9,
            '.menu .container',
            'menu__item'
            
        ).render();
        
        new MenuCard(
            "img/tabs/elite.jpg",
            "elite",
            'Меню “Премиум',
            `В меню “Премиум” мы используем не только красивый дизайн упаковки, но и качественное исполнение блюд. Красная рыба, морепродукты, фрукты - ресторанное меню без похода в ресторан!`,
           14,
            '.menu .container',
            
            
        ).render();
        new MenuCard(
            "img/tabs/post.jpg",
            "post",
            'Меню "Постное"',
            `Меню “Постное” - это тщательный подбор ингредиентов: полное отсутствие продуктов животного происхождения, молоко из миндаля, овса, кокоса или гречки, правильное количество белков за счет тофу и импортных вегетарианских стейков.`,
            22,
            '.menu .container',
            
            
        ).render();



        //----slides (в процессе :( )

        // let slideIndex=1;
        // showSlides(slideIndex)

        // function nextSlide(){
        //     showSlides(slideIndex+=1)


        // }

        // function previousSlide(){
        //     showSlides(slideIndex-=1)
        // }

        // function currentSlide(){
        //     showSlides(n)
        // }

        // function showSlides(n){
        //     let slide = document.getElementsByClassName('offer__slide');

        //     if (n> slides.length){
        //         slideIndex=1

        //     }
        //     if(n<1){
        //         slideIndex=slide.length
        //     }

        //    for (let slide of slides ){
        //     slide.style.display = 'none';

        //    }
        //    slide[slideIndex-1].style.display='block'

        // }

});



    
